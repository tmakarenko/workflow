var gulp           = require('gulp'),
    sass           = require('gulp-sass'),
    browserSync    = require('browser-sync'),
    notify         = require("gulp-notify"),
    autoprefixer   = require('gulp-autoprefixer'),
    babel = require('gulp-babel');

gulp.task('common-js', function() {
	return gulp.src([
		'src/js/main.js',
		])
	.pipe(babel({
            presets: ['es2015']
        }))
	.pipe(gulp.dest('dist/js'))
	.pipe(browserSync.reload({stream: true}));
});
gulp.task('sass', function() {
	return gulp.src('src/sass/**/*.sass')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(gulp.dest('dist/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'dist'
		},
		notify: false,
		// tunnel: true,
		// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
	});
});

gulp.task('watch', ['sass', 'browser-sync'], function() {
	gulp.watch('src/sass/**/*.sass', ['sass']);
	gulp.watch(['src/js/main.js'], ['common-js']);
	gulp.watch('dist/*.html', browserSync.reload);
});
gulp.task('default', ['watch']);